<?php

namespace App\Form;

use App\Entity\SlotTime;
use App\Entity\CallRequest;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class CallRequestType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $date = new \DateTime();
        // $test = $date->format('Y-m-d H:i:s');

        $builder
            ->add('name', TextType::class)
            ->add('first_name', TextType::class)
            ->add('email', EmailType::class)
            ->add('phone', NumberType::class)
            ->add('recall_date', DateType::class)
            ->add('message', TextareaType::class)
            ->add('slot', EntityType::class, [
                'class' => SlotTime::class,
                'choice_label' => 'slot', ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => CallRequest::class,
        ]);
    }
}