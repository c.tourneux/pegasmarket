<?php 

namespace App\Validator\Constraints;

use \Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class IsEmailValidator extends ConstraintValidator
{

    /**
     *
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint): void
    {

        if (null === $value || '' === $value) {
            return;
        }

        $regex = "/^([a-zA-Z0-9\.]+@+[a-zA-Z]+(\.)+[a-zA-Z]{2,3})$/";
        if (!preg_match($regex, $value)) {
            $this->context->buildViolation($constraint->message)
            ->addViolation();
        }
    }
}