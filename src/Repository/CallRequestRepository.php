<?php

namespace App\Repository;

use App\Entity\CallRequest;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CallRequest|null find($id, $lockMode = null, $lockVersion = null)
 * @method CallRequest|null findOneBy(array $criteria, array $orderBy = null)
 * @method CallRequest[]    findAll()
 * @method CallRequest[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CallRequestRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CallRequest::class);
    }

    // /**
    //  * @return CallRequest[] Returns an array of CallRequest objects
    //  */
    public function findLastFiveFields()
    {
        return $this->createQueryBuilder('c')
            ->orderBy('c.Created_date', 'DESC')
            ->setMaxResults(5)
            ->getQuery()
            ->getResult()
        ;
    }

    // /**
    //  * @return CallRequest[] Returns an array of CallRequest objects
    //  */
    public function findTodayFields(\Datetime $date)
    {
        $from = new \DateTime($date->format("Y-m-d"));
        $to   = new \DateTime($date->format("Y-m-d"));
    
        return $this->createQueryBuilder("e")
            ->Where('e.recall_date BETWEEN :from AND :to')
            ->setParameter('from', $from )
            ->setParameter('to', $to)
            ->getQuery()
            ->execute()
        ;
    }
    /*
    public function findOneBySomeField($value): ?CallRequest
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}