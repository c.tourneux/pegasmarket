<?php

namespace App\Entity;

use App\Repository\CallRequestRepository;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use App\Validator\Constraints as Constraint;

/**
 * @ORM\Entity(repositoryClass=CallRequestRepository::class)
 */
class CallRequest
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Constraint\IsText
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Constraint\IsText
     */
    private $first_name;

    /**
     * @ORM\Column(type="string", length=255)
     * @Constraint\IsEmail
     */
    private $email;

    /**
     * @ORM\Column(type="integer")
     * @Constraint\IsPhoneNumber
     */
    private $phone;

    /**
     * @ORM\Column(type="date")
     */
    private $recall_date;

    /**
     * @ORM\Column(type="text")
     */
    private $message;

    /**
     * @ORM\ManyToOne(targetEntity=SlotTime::class, inversedBy="callRequests")
     * @ORM\JoinColumn(nullable=false)
     * @Constraint\IsNumber
     */
    private $slot;

    /**
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $Created_date;
    
    public function __construct()
    {
        $this->recall_date = new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->first_name;
    }

    public function setFirstName(string $first_name): self
    {
        $this->first_name = $first_name;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getPhone(): ?int
    {
        return $this->phone;
    }

    public function setPhone(int $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getRecallDate(): ?\DateTimeInterface
    {
        return $this->recall_date;
    }

    public function setRecallDate(\DateTimeInterface $recall_date): self
    {
        $this->recall_date = $recall_date;

        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    public function getSlot(): ?SlotTime
    {
        return $this->slot;
    }

    public function setSlot(?SlotTime $slot): self
    {
        $this->slot = $slot;

        return $this;
    }

    public function getCreatedDate(): ?\DateTimeInterface
    {
        return $this->Created_date;
    }
}