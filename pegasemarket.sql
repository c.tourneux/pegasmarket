-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 18 sep. 2021 à 23:37
-- Version du serveur :  5.7.31
-- Version de PHP : 7.3.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `pegasemarket`
--

-- --------------------------------------------------------

--
-- Structure de la table `call_request`
--

DROP TABLE IF EXISTS `call_request`;
CREATE TABLE IF NOT EXISTS `call_request` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot_id` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` int(11) NOT NULL,
  `recall_date` date NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_date` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EE3216AA59E5119C` (`slot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `call_request`
--

INSERT INTO `call_request` (`id`, `slot_id`, `name`, `first_name`, `email`, `phone`, `recall_date`, `message`, `created_date`) VALUES
(1, 1, 'Martel', 'Alaine', 'Martelalaine@gmail.com', 610203040, '2021-09-18', 'Ceci est un message', '2021-09-18 20:49:30'),
(2, 2, 'Dumoulin', 'Valentine', 'DumoulinValentine@gmail.com', 610203040, '2021-09-24', 'Ceci est un message !', '2021-09-18 20:51:21'),
(3, 1, 'Breton', 'France', 'BretonFrance@gmail.com', 610203040, '2021-11-18', 'Ceci est un message !', '2021-09-18 20:52:11'),
(4, 2, 'Lafrenière', 'Brice', 'LafreniereBrice@gmail.com', 610203040, '2021-09-19', 'Ceci est un message !', '2021-09-18 20:53:35'),
(5, 1, 'Galarneau', 'Clementine', 'GalarneauClementine@gmail.com', 610203040, '2021-09-18', 'Ceci est un message !', '2021-09-18 20:54:24'),
(6, 1, 'Charpie', 'Tempeste', 'CharpieTempeste@gmail.com', 610203040, '2021-09-18', 'Ceci est un message !', '2021-09-18 20:57:32'),
(7, 2, 'Goudreau', 'Holly', 'GoudreauHolly@gmail.com', 610203040, '2021-11-06', 'Ceci est un message !', '2021-09-18 20:58:25'),
(8, 2, 'Houle', 'Dielle', 'HouleDielle@gmail.com', 610203040, '2021-08-18', 'Ceci est un message !', '2021-09-18 20:59:14'),
(9, 2, 'Chesnay', 'Morgana', 'ChesnayMorgana@gmail.com', 610203040, '2021-09-30', 'Ceci est un message !', '2021-09-18 20:59:49'),
(10, 1, 'Riquier', 'Brier', 'RiquierBrier@gmail.com', 610203040, '2021-09-18', 'Ceci est un message !', '2021-09-18 21:00:30'),
(11, 1, 'Guzman', 'Allison', 'GuzmanAllison@gmail.com', 610203040, '2023-12-01', 'Ceci est un message', '2021-09-18 21:10:09'),
(20, 1, 'Last', 'Entry', 'lastentry@gmail.fr', 123456789, '2021-09-18', 'eee', '2021-09-18 23:30:13');

-- --------------------------------------------------------

--
-- Structure de la table `doctrine_migration_versions`
--

DROP TABLE IF EXISTS `doctrine_migration_versions`;
CREATE TABLE IF NOT EXISTS `doctrine_migration_versions` (
  `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
  `executed_at` datetime DEFAULT NULL,
  `execution_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `doctrine_migration_versions`
--

INSERT INTO `doctrine_migration_versions` (`version`, `executed_at`, `execution_time`) VALUES
('DoctrineMigrations\\Version20210917191903', '2021-09-17 19:19:11', 59),
('DoctrineMigrations\\Version20210918115635', '2021-09-18 11:57:30', 40);

-- --------------------------------------------------------

--
-- Structure de la table `slot_time`
--

DROP TABLE IF EXISTS `slot_time`;
CREATE TABLE IF NOT EXISTS `slot_time` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `slot` varchar(55) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `slot_time`
--

INSERT INTO `slot_time` (`id`, `slot`) VALUES
(1, 'Le matin entre 9h et 11h'),
(2, 'L\'après-midi entre 14h-16h');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `call_request`
--
ALTER TABLE `call_request`
  ADD CONSTRAINT `FK_EE3216AA59E5119C` FOREIGN KEY (`slot_id`) REFERENCES `slot_time` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
